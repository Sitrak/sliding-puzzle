var img;
var tab = [];
var tabFinal = [];

var widthRect = null;
var heightRect = null;

var vide;
var tmp;
var select;
var btn;

var nbrTuil = 4;
var melange = 200;

var indice1 = null;
var indice2 = null;

var score = 0;

var pX = null;
var pY = null;

function preload() {
    //Charger les images
    img = loadImage("assets/image/Bird.jpg");
    imgVide = loadImage("assets/image/vide.PNG");
}

function setup() {
    //Créer un canvas de taille de l'image
    createCanvas(500, 550);
    img.resize(500, 500);
    //Liste des niveaux
    select = createSelect();
    select.class('form-control');
    select.id('niveau');
    select.position(434, 505);
    select.option('Facile');
    select.option('Normal');
    select.option('Difficile');
    //Crée le bouton Rejouer
    btn = createButton('Rejouer');
    btn.position(765,505);
    btn.class('btn btn-danger');
    textFont("Source Code Pro");
    textSize(18);
    //Calculer la taille d'une piece
    widthRect = img.width / nbrTuil;
    heightRect = img.height / nbrTuil;

    //Ajouter les pieces decoupé dans un tableau 2D
    for (var i = 0; i < nbrTuil; i++) {
        tab[i] = []; //Tableau à manipuler
        tabFinal[i] = []; //Tableau modele
        for (var k = 0; k < nbrTuil; k++) {
            tab[i][k] = img.get(widthRect * k, heightRect * i, widthRect - 2, heightRect - 2);
            tabFinal[i][k] = tab[i][k];
        }
    }
    //Remplacer le dernier piece par une image blanche
    tab[nbrTuil - 1][nbrTuil - 1] = imgVide;
    tabFinal[nbrTuil - 1][nbrTuil - 1] = imgVide;

    //Chercher l'image vide (imgVide)
    findEmpty();
    //Menlanger le puzzle
    shufflePuzzle();
    noLoop();
}

function draw() {
    text(score,200,520);
    //Tracer le tableau tab[] melangé
    for (var i = 0; i < nbrTuil; i++) {
        for (var k = 0; k < nbrTuil; k++) {
            image(tab[i][k], widthRect * k, heightRect * i);
        }
    }

}

function mousePressed() {
    //Recuperer le coordonner x et y du piece selectionner
    pX = Math.floor(mouseX / widthRect);
    pY = Math.floor(mouseY / widthRect);
    //Garder temporirement le piece selectionner
    tmp = tab[pY][pX];
    //Tester si le piece selectionner peut etre echanger par le piece vide
    if (pY === indice1 && pX === (indice2 - 1) || pY === indice1 && pX === (indice2 + 1) ||
            pY === (indice1 - 1) && pX === indice2 || pY === (indice1 + 1) && pX === indice2) {
        //Changer le piece selectionnet par le piece vide (indice1 et indice2 sont les coordonnes du vide)
        tab[pY][pX] = tab[indice1][indice2];
        //Changer le piece vide par le piece selectionner garder temporairement
        tab[indice1][indice2] = tmp;
        score++;
    }
    //Chercher l'image vide (imgVide)
    //console.log(CheckIfDone());
    findEmpty();
    draw();
//console.log(mouseX+"  "+mouseX / widthRect);

}

/**
 * Chercher l'image vide et ajouter les coordonnes (x,y)
 * dans les variables indice1 et indice2
 */
function findEmpty() {
    for (var i = 0; i < nbrTuil; i++) {
        for (var k = 0; k < nbrTuil; k++) {
            if (tab[i][k] === imgVide) {
                indice1 = i;
                indice2 = k;
            }
        }
    }
}


/**
 * Melanger les pieces en deplacant le piece voisin choisi par hasard
 */
function shufflePuzzle() {
    //Declarer une variable compteur pour compter le deplacement
    var compteur = 0;
    //Tant que le deplacement est inferieur a 20
    while (compteur < melange) {
        //Choisir au hasard une piece
        var pXRand = Math.floor(Math.random() * nbrTuil);
        var pYRand = Math.floor(Math.random() * nbrTuil);
        tmp = tab[pYRand][pXRand];
        //Tester si cette piece est à côter du vide
        if (pYRand === indice1 && pXRand === (indice2 - 1) || pYRand === indice1 && pXRand === (indice2 + 1) ||
                pYRand === (indice1 - 1) && pXRand === indice2 || pYRand === (indice1 + 1) && pXRand === indice2) {
            //Echanger les deux pieces
            tab[pYRand][pXRand] = tab[indice1][indice2];
            tab[indice1][indice2] = tmp;
            findEmpty();
            //incrementer le compteur
            compteur++;
        }
    }
}

/**
 * Tester si le jeux est fini
 */
function checkIfDone() {
    for (var i = 0; i < nbrTuil; i++) {
        for (var k = 0; k < nbrTuil; k++) {
            //  Comparer le tableau manipuler (tab[]) avec le tableau d'origine (tabFinal[])
            if (tab[i][k] !== tabFinal[i][k]) {
                return false;
            }
        }
    }
    return true;
}